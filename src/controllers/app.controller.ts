import { Controller, Get } from '@nestjs/common';

import { Resources } from '../resources';

@Controller()
export class AppController {
  @Get()
  root() {
    return 'Running Teszt!';
  }

  @Get('/resources')
  resources() {
    return Resources;
  }
}
//superadmin admin, moderator, user, visitor
// propertitt lásson
